export default class Router {
	static titleElement;
	static contentElement;
	static #menuElement; // propriété statique privée
	/**
	 * Tableau des routes/pages de l'application.
	 * @example `Router.routes = [{ path: '/', page: pizzaList, title: 'La carte' }]`
	 */
	static routes = [];

	/**
	 * Affiche la page correspondant à `path` dans le tableau `routes`
	 * @param {String} path URL de la page à afficher
	 */
	static navigate(path) {
		const route = this.routes.find(route => route.path === path);
		if (route) {
			// affichage du titre de la page
			this.titleElement.innerHTML = `<h1>${route.title}</h1>`;
			// affichage de la page elle même
			this.contentElement.innerHTML = route.page.render();
		}
	}

	/**
	 * Indique au Router la balise HTML contenant le menu de navigation
	 * Écoute le clic sur chaque lien et déclenche la méthode navigate
	 * @param element Élément HTML qui contient le menu principal
	 */
	static set menuElement(element) {
		// setter
		this.#menuElement = element;
		// au clic sur n'importe quel lien (<a href>) contenu dans "element"
		// déclenchez un appel à Router.navigate(path)
		// où "path" est la valeur de l'attribut `href=".."` du lien cliqué

		const lien = element.querySelector('.pizzaFormLink');
		lien.addEventListener('click', event => {
			event.preventDefault();
			Router.navigate(lien.getAttribute('href'));
		});
	}
}
