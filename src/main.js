import Router from './Router';
import data from './data';
import PizzaList from './pages/PizzaList';
import Component from './components/Component';

function closeButton() {
	const sel = document.querySelector('.newsContainer');
	sel.setAttribute('style', 'display:none');
}

Router.titleElement = document.querySelector('.pageTitle');
Router.contentElement = document.querySelector('.pageContent');

//const pizzaList = new PizzaList([]);
//Router.routes = [{ path: '/', page: pizzaList, title: 'La carte' }];

// Router.navigate('/'); // affiche une page vide
// pizzaList.pizzas = data; // appel du setter
// Router.navigate('/'); // affiche la liste des pizzas

const html = '<small>les pizza c est la vie</small>';

const logo = document.querySelector('.logo');

logo.innerHTML += html;

const secSelector = document.querySelector('.newsContainer');
secSelector.setAttribute('style', '');

const bouton = document.querySelector('.closeButton');
bouton.addEventListener('click', closeButton);

const pizzaList = new PizzaList(data),
	aboutPage = new Component('section', null, 'Ce site est génial'),
	pizzaForm = new Component(
		'section',
		null,
		'Ici vous pourrez ajouter une pizza'
	);

Router.routes = [
	{ path: '/', page: pizzaList, title: 'La carte' },
	{ path: '/a-propos', page: aboutPage, title: 'À propos' },
	{ path: '/ajouter-pizza', page: pizzaForm, title: 'Ajouter une pizza' },
];
console.log('alerte');
Router.menuElement = document.querySelector('.mainMenu');
